# DFL Notifications
DFL::NotifyWatcher is a dbus implementation of the XDG Desktop Notification specifications.
This class watches for the incoming notification requests from the client and relays them to the notification manager.
DFL::Notification is a convenience class that helps clients to create a notification. It uses dbus to relay the
notification to the notification daemon.


### Dependencies:
* <tt>Qt5 (qtbase5-dev, qtbase5-dev-tools)</tt>
* <tt>meson   (For configuring the project)</tt>
* <tt>ninja   (To build the project)</tt>


### Notes for compiling - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/desktop-frameworks/notification.git dfl-notification`
- Enter the `dfl-notification` folder
  * `cd dfl-notification`
- Configure the project - we use meson for project management
  * `meson .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* Please test and let us know


### Upcoming
* Any other feature you request for... :)
