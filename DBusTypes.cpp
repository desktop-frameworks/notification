/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *
 * DFL::NotifyWatcher is a dbus implementation of the XDG Desktop
 * Notification specifications. This class watches for the incoming
 * notification requests from the client and relays them to the
 * notification manager.
 **/

#include "DFNotifyWatcher.hpp"

QDBusArgument &operator<<( QDBusArgument& argument, const DFL::NotifyWatcher::Notification& n ) {
    argument.beginStructure();
    argument << n.appName;
    argument << n.id;
    argument << n.appIcon;
    argument << n.summary;
    argument << n.body;
    argument << n.actions;
    argument << n.hints;
    argument << n.timeout;
    argument.endStructure();

    return argument;
}


const QDBusArgument &operator>>( const QDBusArgument& argument, DFL::NotifyWatcher::Notification& n ) {
    argument.beginStructure();
    argument >> n.appName;
    argument >> n.id;
    argument >> n.appIcon;
    argument >> n.summary;
    argument >> n.body;
    argument >> n.actions;
    argument >> n.hints;
    argument >> n.timeout;
    argument.endStructure();

    return argument;
}


bool operator==( const DFL::NotifyWatcher::Notification& lhs, const DFL::NotifyWatcher::Notification& rhs ) {
    if ( lhs.appName != rhs.appName ) {
        return false;
    }

    if ( lhs.id != rhs.id ) {
        return false;
    }

    if ( lhs.appIcon != rhs.appIcon ) {
        return false;
    }

    if ( lhs.summary != rhs.summary ) {
        return false;
    }

    if ( lhs.body != rhs.body ) {
        return false;
    }

    if ( lhs.actions != rhs.actions ) {
        return false;
    }

    if ( lhs.hints.keys() != rhs.hints.keys() ) {
        return false;
    }

    if ( lhs.hints.values() != rhs.hints.values() ) {
        return false;
    }

    if ( lhs.timeout != rhs.timeout ) {
        return false;
    }

    return true;
}
