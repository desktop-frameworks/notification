/**
 * Copyright (c) 2022
 *    Marcus Britanicus (https://gitlab.com/marcusbritanicus)
 *    Abrar (https://gitlab.com/s96abrar)
 *
 * DFL::NotifyWatcher is a dbus implementation of the XDG Desktop
 * Notification specifications. This class watches for the incoming
 * notification requests from the client and relays them to the
 * notification manager.
 **/

#include "DFNotifyWatcher.hpp"

QString DFL::NotifyWatcher::notifyFunction;

const QString service    = "org.freedesktop.Notifications";
const QString objectPath = "/org/freedesktop/Notifications";
const QString interface  = "org.freedesktop.Notifications";

DFL::NotifyWatcher::NotifyWatcher( QString functionName, QObject *parent ) : QDBusAbstractAdaptor( parent ) {
    setAutoRelaySignals( true );

    /** Function name */
    notifyFunction = functionName;

    QDBusConnection dbusSess   = QDBusConnection::sessionBus();
    bool            registered = true;
    registered &= dbusSess.registerObject( objectPath, parent );
    registered &= dbusSess.registerService( service );

    if ( not registered ) {
        qCritical() << "The service org.freedesktop.Notifications cannot be registered.";
        qCritical() << "Is another notification daemon running?";
        qFatal( "Aborting..." );
    }

    else {
        qInfo() << "The service org.freedesktop.Notifications is ready and running.";
    }
}


DFL::NotifyWatcher::~NotifyWatcher() {
}


void DFL::NotifyWatcher::setAvailableHints( QStringList hints ) {
    mAvailableHints = hints;
}


void DFL::NotifyWatcher::setCapabilities( QStringList caps ) {
    mCapabilities = caps;
}


void DFL::NotifyWatcher::setServerInformation( QStringList info ) {
    mServerInfo = info;

    if ( mServerInfo.count() < 4 ) {
        qWarning() << "Invalid server information. Please provide the following details";
        qWarning() << "  1. Application Name";
        qWarning() << "  2. Vendor";
        qWarning() << "  3. Application version";
        qWarning() << "  4. Specification version";
    }

    /** To ensure we have 4 strings. */
    while ( mServerInfo.count() < 4 ) {
        mServerInfo << "";
    }
}


void DFL::NotifyWatcher::emitActionInvoked( uint id, QString action ) {
    QDBusMessage msg = QDBusMessage::createSignal( objectPath, interface, "ActionInvoked" );

    msg << id << action;
    QDBusConnection::sessionBus().send( msg );
}


void DFL::NotifyWatcher::emitNotificationClosed( uint id, uint reason ) {
    QDBusMessage msg = QDBusMessage::createSignal( objectPath, interface, "NotificationClosed" );

    msg << id << reason;
    QDBusConnection::sessionBus().send( msg );
}


void DFL::NotifyWatcher::emitNotificationReplied( uint id, QString reply ) {
    QDBusMessage msg = QDBusMessage::createSignal( objectPath, interface, "NotificationReplied" );

    msg << id << reply;
    QDBusConnection::sessionBus().send( msg );
}


uint DFL::NotifyWatcher::Notify( QString app_name, uint replaces_id, QString app_icon, QString summary,
                                 QString body, QStringList actions, QVariantMap hints, int expire_timeout ) {
    uint reply;

    DFL::NotifyWatcher::Notification notify {
        app_name,
        replaces_id,
        app_icon,
        summary,
        body,
        actions,
        hints,
        expire_timeout
    };

    QMetaObject::invokeMethod(
        parent(),
        notifyFunction.toUtf8().constData(),
        Q_RETURN_ARG( uint, reply ),
        Q_ARG( DFL::NotifyWatcher::Notification, notify )
    );

    return reply;
}


void DFL::NotifyWatcher::CloseNotification( uint id ) {
    emit CloseNotificationRequest( id );
}


QStringList DFL::NotifyWatcher::GetAvailableHints() {
    return mAvailableHints;
}


QStringList DFL::NotifyWatcher::GetCapabilities() {
    return mCapabilities;
}


QString DFL::NotifyWatcher::GetServerInformation( QString& vendor, QString& version, QString& specVersion ) {
    vendor      = mServerInfo.value( 1 );
    version     = mServerInfo.value( 2 );
    specVersion = mServerInfo.value( 3 );

    return mServerInfo.value( 0 );
}


bool DFL::NotifyWatcher::isNotifierHostRegistered() {
    return true;
}


void DFL::NotifyWatcher::RegisterNotifierHost( const QString& service ) {
    Q_UNUSED( service );
}
